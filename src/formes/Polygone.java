/**
 * BONIJOL Pierre & LECOCQ Coraline
 */

package formes;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

import Controles.Paint;

public class Polygone implements Formes {
    private List<Point> points = new ArrayList<>();
    /**
     * x et y de la forme
     */
    private Point location;
    /**
     * w et h de la forme
     */
    private Dimension dimensions;
    /**
     * couleur de la forme ( par defaut noir)
     */
    private Color color;

    /**
     * couleur du remplissage
     */

    private Color fillColor = null; // null par défault
    /**
     * nombre de cotés du polygone
     */
    private int NbCote;

    public Polygone() {

        this.color = Paint.couleurSelect;


    }


    private void initialisation() {
        points.clear();
        double theta = Math.toRadians(360 / NbCote);
        points.add(0, new Point(location.x + dimensions.width, location.y + dimensions.height));

        for (int i = 0; i < NbCote - 1; i++) {
            int x = (int) (location.x + (points.get(i).x - location.x) * Math.cos(theta)
                    - (points.get(i).y - location.y) * (Math.sin(theta)));
            int y = (int) (location.y + (points.get(i).x - location.x) * Math.sin(theta)
                    + (points.get(i).y - location.y) * (Math.cos(theta)));
            points.add(new Point(x, y));
        }
    }

    private Polygone(List<Point> points) {
        this.color = Paint.couleurSelect;
        this.points = points;
        NbCote = points.size();
    }
    public void setCotes(int cotes)
    {
        NbCote = cotes;
        initialisation();
    }

    @Override
    public void draw(Graphics g) {
        int nbpoints = points.size();
        int[] xp = new int[nbpoints];
        int[] yp = new int[nbpoints];
        for (int i = 0; i < nbpoints; i++) {
            xp[i] = points.get(i).x;
            yp[i] = points.get(i).y;
        }

        if (fillColor != null && !Paint.getPainterPanel().grise) {
            g.setColor(fillColor);
            g.fillPolygon(xp, yp, nbpoints);
        }
        g.setColor(color);
        g.drawPolygon(xp, yp, nbpoints);
        if (Paint.getPainterPanel().grise) {
            g.setColor(new Color(0, 0, 0, 20));
            g.fillPolygon(xp, yp, nbpoints);

        }
    }

    public boolean contain(int x, int y) {
        int numberOfPoints = points.size();
        int[] xPoints = new int[numberOfPoints];
        int[] yPoints = new int[numberOfPoints];
        for (int i = 0; i < numberOfPoints; i++) {
            xPoints[i] = points.get(i).x;
            yPoints[i] = points.get(i).y;
        }
        java.awt.Polygon p = new java.awt.Polygon(xPoints, yPoints, numberOfPoints);
        return p.contains(x, y);
    }

    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public Color getColor() {
        return color;
    }

    @Override
    public void setFillColor(Color color) {
        fillColor = color;
    }

    @Override
    public Color getFillColor() {
        return fillColor;
    }

    @Override
    public Object clone() {
        Polygone polyg = new Polygone(points);
        polyg.setColor(this.getColor());
        polyg.setFillColor(this.getFillColor());
        return polyg;
    }

    @Override
    public void setProprietes(Point location,Dimension dimensions) {
        this.location = location;
        this.dimensions = dimensions;

    }

    @Override
    public void setLocation(Point location) {
        this.location = location;
    }

    @Override
    public Point getLocation() {
        return location;
    }

    @Override
    public Dimension getDimensions() {
        return dimensions;
    }

    @Override
    public void setDimensions(Dimension dimensions) {
        this.dimensions = dimensions;
    }

    @Override
    public void move(int x1, int y1, int x2, int y2) {
        location.x += x2 - x1;
        location.y += y2 - y1;
        initialisation();

    }

    @Override
    public void redimensionner(int x1, int y1, int x2, int y2) {
        dimensions.width += x2 - x1;
        dimensions.height += y2 - y1;
        initialisation();
    }
}