/**
 * BONIJOL Pierre & LECOCQ Coraline
 */

package formes;

import java.awt.*;

import Controles.Paint;

public class Ligne implements Formes, Cloneable {
    /**
     * x et y de la forme
     */
    private Point location;
    /**
     * w et h de la forme
     */
    private Dimension dimensions;
    /**
     * couleur de la forme ( par defaut noir)
     */
    private Color color;

    /**
     * couleur du remplissage
     */

    private Color fillColor = null;

    public Ligne() {
        color = Paint.couleurSelect;



    }


    @Override
    public void draw(Graphics g) {
        g.setColor(getColor());
        g.drawLine(location.x, location.y, location.x + dimensions.width, location.y + dimensions.height);
        if (Paint.getPainterPanel().grise) {
            g.setColor(Color.LIGHT_GRAY);
            g.drawLine(location.x, location.y, location.x + dimensions.width, location.y + dimensions.height);

        }

    }

    @Override
    public void setLocation(Point location) {
        this.location = location;

    }

    @Override
    public Point getLocation() {
        return location;
    }

    @Override
    public Dimension getDimensions() {
        return dimensions;
    }

    @Override
    public void setDimensions(Dimension dimensions) {
        this.dimensions = dimensions;

    }

    @Override
    public void setProprietes(Point location,Dimension dimensions) {
        this.location = location;
        this.dimensions = dimensions;

    }

    @Override
    public boolean contain(int x, int y) {

        if (x <= Math.max(location.x, location.x + dimensions.width)
                && x >= Math.min(location.x, location.x + dimensions.width))
            try {


                return (((x - location.x) / (y - location.y)) == (location.x + dimensions.width - location.x)
                        / (location.y + dimensions.height - location.y));
            } catch (Exception exp) {

                return (Math.abs(Math.ceil((x - location.x) * ((float)dimensions.height / 10))
                        - Math.ceil(y - location.y) * ((float)dimensions.width / 10)) <= 400);

            }

        return false;
    }
    @Override
    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public Color getColor() {
        return color;
    }

    @Override
    public void setFillColor(Color color) {
        this.color = color;
    }

    @Override
    public Color getFillColor() {
        return fillColor;
    }

    @Override
    public void move(int x1, int y1, int x2, int y2) {
        location.x += x2 - x1;
        location.y += y2 - y1;
    }

    @Override
    public void redimensionner(int x1, int y1, int x2, int y2) {

        dimensions.width = (dimensions.width + x2 - x1);
        dimensions.height = (dimensions.height + y2 - y1);
    }

    @Override
    public Object clone() {
        Ligne formeClone = new Ligne();
        formeClone.setLocation(this.getLocation());
        formeClone.setDimensions(this.getDimensions());
        formeClone.setColor(this.getColor());
        formeClone.setFillColor(this.getFillColor());
        return formeClone;
    }

}
