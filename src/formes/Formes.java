/**
 * BONIJOL Pierre & LECOCQ Coraline
 */

package formes;

import java.awt.*;


public interface Formes {

    /**
     *   Dessiner la forme
     */

    void draw(Graphics g);

    /**
     *   Mettre la position x et y
     */

    void setLocation(Point location);

    /**
     *   Recuperer position x et y
     */

    Point getLocation();

    /**
     *   Recuperer dimensions w et h d ela forme
     */

    Dimension getDimensions();

    /**
     *   Pour l'ouverture de fichier, assigner au bon objet
     */

    static Formes formesType(final String formeNom) {
        try {
            return (Formes) Class.forName(formeNom).newInstance();
        } catch (final Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     *   Mettre les dimensions w et h
     */

    void setDimensions(Dimension dimensions);

    /**
     *   Hitbox de la forme
     */

    boolean contain(int x, int y);

    /**
     *   Mettre la couleur
     */

    void setColor(Color color);

    /**
     *   Recuperer la couleur
     */

    Color getColor();

    /**
     *   Mettre la couleur de remplissage
     */

    void setFillColor(Color color);

    /**
     *   Recupérer le couleur de remplissage
     */

    Color getFillColor();

    /**
     *   Retourne une copie de la forme
     */

    Object clone() throws CloneNotSupportedException;

    /**
     *   Deplacer la forme
     */

    void move(int x1, int y1, int x2, int y2);

    /**
     *   Redimensionner la forme
     */

    void redimensionner(int x1, int y1, int x2, int y2);

    /**
     *   Ajoutes toutes les propiétés des fomes ( x,y , w, h)
     */
     void setProprietes(Point location,Dimension dimensions);

}
