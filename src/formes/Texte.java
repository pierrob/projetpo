/**
 * BONIJOL Pierre & LECOCQ Coraline
 */

package formes;

import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;

import Controles.Paint;


public class Texte implements Formes, Cloneable {

    /**
     * x et y de la forme
     */
    private Point location;
    /**
     * w et h de la forme
     */
    private Dimension dimensions;
    /**
     * couleur de la forme ( par defaut noir)
     */
    private Color color;

    /**
     * couleur du remplissage
     */

    private Color fillColor = null;

    /**
     * Calcul hitbox du texte
     */
    private Dimension calculTexte;


    /**
     * Texte entré par l'utilisateur
     */
    private String textechoisi;


    public Texte() {
        color = Paint.couleurSelect;
        this.calculTexte = new Dimension();
    }


    @Override
    public void draw(Graphics g) {
        g.setColor(getColor());
        AffineTransform affinetransform = new AffineTransform();
        FontRenderContext frc = new FontRenderContext(affinetransform,true,true);
        Font myFont = new Font ("Arial", Font.BOLD, dimensions.width);


        calculTexte.width = (int)(myFont.getStringBounds(textechoisi, frc).getWidth());
        calculTexte.height = (int)(myFont.getStringBounds(textechoisi, frc).getHeight());

        g.setFont (myFont);
        g.drawString(textechoisi,location.x,location.y);
        if (Paint.getPainterPanel().grise) {
            g.setColor(Color.LIGHT_GRAY);
            myFont = new Font ("Arial", Font.BOLD, dimensions.width);


            calculTexte.width = (int)(myFont.getStringBounds(textechoisi, frc).getWidth());
            calculTexte. height = (int)(myFont.getStringBounds(textechoisi, frc).getHeight());
            g.setFont (myFont);
            g.drawString(textechoisi,location.x,location.y);

        }


    }


    @Override
    public void setLocation(Point location) {
        this.location = location;

    }

    @Override
    public Point getLocation() {
        return location;
    }

    @Override
    public Dimension getDimensions() {
        return dimensions;
    }

    @Override
    public void setDimensions(Dimension dimensions) {
        this.dimensions = dimensions;

    }

    @Override
    public void setProprietes(Point location,Dimension dimensions) {
        this.location = location;
        this.dimensions = dimensions;

    }

    public void setTexte(String txt)
    {
        this.textechoisi = txt;
    }

    public String getTexte()
    {
        return this.textechoisi;
    }

    @Override
    public boolean contain(int x, int y) {
        return x >= location.x && x <= (location.x + calculTexte.width) && y <= location.y
                && y >= (location.y - calculTexte.height);
    }
    @Override
    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public Color getColor() {
        return color;
    }

    @Override
    public void setFillColor(Color color) {
        this.color = color;
    }

    @Override
    public Color getFillColor() {
        return fillColor;
    }

    @Override
    public void move(int x1, int y1, int x2, int y2) {
        location.x += x2 - x1;
        location.y += y2 - y1;
    }

    @Override
    public void redimensionner(int x1, int y1, int x2, int y2) {
        dimensions.width = (dimensions.width + x2 - x1);
        dimensions.height = (dimensions.height + y2 - y1);
    }

    @Override
    public Object clone() {
        Texte formeClone = new Texte();
        formeClone.setLocation(this.getLocation());
        formeClone.setDimensions(this.getDimensions());
        formeClone.setColor(this.getColor());
        formeClone.setFillColor(this.getFillColor());
        return formeClone;
    }

}
