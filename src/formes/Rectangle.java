/**
 * BONIJOL Pierre & LECOCQ Coraline
 */

package formes;

import java.awt.*;

import Controles.Paint;

public class Rectangle implements Formes {

    /**
     * x et y de la forme
     */
    private Point location;
    /**
     * w et h de la forme
     */
    private Dimension dimensions;
    /**
     * couleur de la forme ( par defaut noir)
     */
    private Color color;

    /**
     * couleur du remplissage
     */

    private Color fillColor = null;

    public Rectangle() {
        super();
        color = Paint.couleurSelect;
    }

    @Override
    public void draw(Graphics g) {
        if (fillColor != null&&!Paint.getPainterPanel().grise) {
            g.setColor(fillColor);
            g.fillRect(location.x, location.y, dimensions.width, dimensions.height);
        }
        g.setColor(color);
        g.drawRect(location.x, location.y, dimensions.width < 0 ? 0 : dimensions.width,
                dimensions.height < 0 ? 0 : dimensions.height);
        if (Paint.getPainterPanel().grise) {
            g.setColor(new Color(0, 0, 0, 20));
            g.fillRect(location.x, location.y, dimensions.width, dimensions.height);
        }

    }


    @Override
    public boolean contain(int x, int y) {
        return x >= location.x && x <= (location.x + dimensions.width) && y >= location.y
                && y <= (location.y + dimensions.height);
    }

    @Override
    public void setLocation(Point location) {
        this.location = location;
    }

    @Override
    public Point getLocation() {
        return location;
    }

    @Override
    public Dimension getDimensions() {
        return dimensions;
    }

    @Override
    public void setProprietes(Point location,Dimension dimensions) {
        this.location = location;
        this.dimensions = dimensions;

    }

    @Override
    public void setDimensions(Dimension dimensions) {
        this.dimensions = dimensions;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public Color getColor() {

        return color;
    }

    public void setFillColor(Color color) {
        fillColor = color;
    }

    public Color getFillColor() {
        return fillColor;
    }

    @Override
    public void move(int x1, int y1, int x2, int y2) {
        location.x += x2 - x1;
        location.y += y2 - y1;
    }

    @Override
    public void redimensionner(int x1, int y1, int x2, int y2) {
        dimensions.width = (dimensions.width + x2 - x1);
        dimensions.height = (dimensions.height + y2 - y1);
    }

    @Override
    public Object clone() {
        Rectangle rectangle = new Rectangle();
        rectangle.setLocation(this.getLocation());
        rectangle.setDimensions(this.getDimensions());
        rectangle.setColor(this.getColor());
        rectangle.setFillColor(this.getFillColor());
        return rectangle;
    }

}
