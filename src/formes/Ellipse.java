/**
 * BONIJOL Pierre & LECOCQ Coraline
 */

package formes;

import java.awt.*;
import java.awt.geom.Ellipse2D;

import Controles.Paint;


public class Ellipse implements Formes {
    /**
     * x et y de la forme
     */
    private Point location;
    /**
     * w et h de la forme
     */
    private Dimension dimensions;
    /**
     * couleur de la forme ( par defaut noir)
     */
    private Color color;

    /**
     * couleur du remplissage : null
     */

    private Color fillColor = null;

    public Ellipse() {
        color = Paint.couleurSelect;

    }

    @Override
    public void draw(Graphics g) {

        if (getFillColor() != null&&! Paint.getPainterPanel().grise) {
            g.setColor(getFillColor());
            g.fillOval(location.x, location.y, dimensions.width, dimensions.height);
        }
        g.setColor(getColor());
        g.drawOval(location.x, location.y, dimensions.width < 0 ? 0 : dimensions.width,
                dimensions.height < 0 ? 0 : dimensions.height);
        if (Paint.getPainterPanel().grise) {
            g.setColor(new Color(0, 0, 0, 20));
            g.fillOval(location.x, location.y, dimensions.width, dimensions.height);
        }
    }

    @Override
    public Dimension getDimensions() {
        return dimensions;
    }

    @Override
    public void setDimensions(Dimension dimensions) {
        this.dimensions = dimensions;
    }

    @Override
    public void setLocation(Point location) {
        this.location = location;
    }

    @Override
    public Point getLocation() {
        return location;
    }

    public boolean contain(int x, int y) {
        final Ellipse2D el = new Ellipse2D.Float(location.x, location.y, dimensions.width < 0 ? 0 : dimensions.width,
                dimensions.height < 0 ? 0 : dimensions.height);
        return el.contains(x, y);
    }

    @Override
    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public Color getColor() {
        return color;
    }

    @Override
    public void setFillColor(Color color) {
        fillColor = color;
    }

    @Override
    public Color getFillColor() {
        return fillColor;
    }

    public void move(int x1, int y1, int x2, int y2) {
        location.x += x2 - x1;
        location.y += y2 - y1;
    }

    @Override

    public void redimensionner(int x1, int y1, int x2, int y2) {
        dimensions.width = (dimensions.width + x2 - x1);
        dimensions.height = (dimensions.height + y2 - y1);
    }

    @Override
    public void setProprietes(Point location,Dimension dimensions) {
        this.location = location;
        this.dimensions = dimensions;

    }

    @Override
    public Object clone() {
        Ellipse formeClone = new Ellipse();
        formeClone.setLocation(this.getLocation());
        formeClone.setDimensions(this.getDimensions());
        formeClone.setFillColor(this.getFillColor());
        formeClone.setColor(this.getColor());
        return formeClone;
    }
}
