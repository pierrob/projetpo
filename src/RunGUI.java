/**
 * BONIJOL Pierre & LECOCQ Coraline
 */
import Vues.MainGUI;

import java.awt.*;
class RunGUI {
    public static void main(String args[]) {
        EventQueue.invokeLater(new Runnable() {
            /**
             * lancer notre application
             */
            public void run() {
                new MainGUI();
            }
        });
    }
}
