/**
 * BONIJOL Pierre & LECOCQ Coraline
 */

package exceptions;


public class CannotRedoException extends Exception {
    @Override
    public String toString() {
        return "Operation impossible, rien a redo!";
    }
}
