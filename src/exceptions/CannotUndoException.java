/**
 * BONIJOL Pierre & LECOCQ Coraline
 */

package exceptions;

public class CannotUndoException extends Exception {
    
    @Override
    public String toString() {
        return "Operation invalide, rien a undone!";
    }
}
