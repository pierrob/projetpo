/**
 * BONIJOL Pierre & LECOCQ Coraline
 */

package exceptions;

public class HistoryIsEmptyException extends Exception {
    @Override
    public String toString() {
        return "Operation invalide, l'historique est vide!";
    }
}
