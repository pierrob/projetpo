/**
 * BONIJOL Pierre & LECOCQ Coraline
 */

package Controles;

import Vues.BarCoord;

import formes.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.List;

import Models.CopierColler;
import Models.Model;
import Models.OuvertureFichiers;

import Vues.MainGuiVue;

import formes.Rectangle;


public class Paint extends JPanel implements MouseListener, MouseMotionListener {


    private int ancienX; // premier x

    private int ancienY; // premier y

    private int currentX; //x actuel

    private int currentY; // y actuel

    private static Paint PaintPan;

    static int PolygoneCotes = 5; // nombre cotés polygone

    static String texteentered = "test";

    private Formes formeActuelle; // la derniere forme a avoir reçue une operation

    public static Color couleurSelect = Color.black; // couleur selectionnée

    static Formes formeSelectionne = null; //forme selectionnée avec l'outil

    /**
     * Permet d'afficher la forme en grise lorsqu'elle est selectionnée
     */

    public boolean grise = false;

    /**
     * Indique qu'il faut mettre à jour l'historique
     */
    private boolean a_change = false;


    private Formes formeCopie; // la forme copiée

    private int xCliqueDroit = 0; // x clique droit pour copier/coller

    private int yCliqueDroit = 0; // y clique droit pour copier/coller


    private Paint() {
        this.addMouseListener(this);
        this.addMouseMotionListener(this);
        CopierColler rightClickPopUpMenu = new CopierColler();
        final JMenuItem menuItem = (JMenuItem) rightClickPopUpMenu.getComponents()[0];

        menuItem.addActionListener(e -> {
            switch (menuItem.getText()) {
                case "Copier":
                    for (Formes forme : Model.getModel().getFormes()) {
                        if (forme.contain(xCliqueDroit, yCliqueDroit)) {
                            try {
                                formeCopie = (Formes) forme.clone();
                                menuItem.setText("Coller");
                            } catch (CloneNotSupportedException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }
                    break;
                case "Coller":
                    formeCopie.setLocation(new Point(xCliqueDroit, yCliqueDroit));
                    try {
                        Model.getModel().getFormes().add((Formes) formeCopie.clone());
                        Model.getModel().getHistorique().update();
                        menuItem.setText("Copier");
                    } catch (CloneNotSupportedException e1) {
                        e1.printStackTrace();
                    }
                    repaint();
                    break;
            }
        });

        setComponentPopupMenu(rightClickPopUpMenu);
    }

    /**
     * retourne l'objet Paint
     */
    public static Paint getPainterPanel() {
        if (PaintPan == null) {
            PaintPan = new Paint();
        }
        return PaintPan;
    }

    /**
     * peindre le composant du programme et gérer chaque état
     */

    private static String OS = System.getProperty("os.name").toLowerCase();

    private static boolean isWindows() {

        return (OS.contains("win"));

    }

    private static boolean isMac() {

        return (OS.contains("mac"));

    }

    private static boolean isUnix() {

        return (OS.contains("nix") || OS.contains("nux") || OS.indexOf("aix") > 0 );

    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        if (OuvertureFichiers.image != null) {
            g.drawImage(OuvertureFichiers.image, 0, 0, null);
            repaint();
        }



        if(MainGuiVue.dessinfield == 1)
        {


            int x,y,w,h;

            x =  MainGuiVue.xfield;
            y =  MainGuiVue.yfield;
            w =  MainGuiVue.wfield;
            h =  MainGuiVue.hfield;

            if ( x != 0 && y != 0 && w != 0 && h !=0) //dessins des formes avec jtextfield
            {
                List<Formes> formes = Model.getModel().getFormes();
                switch (MainGuiVue.getOperation()) {
                    case Polygone:
                        Polygone Rpolygon = new Polygone();
                        Rpolygon.setProprietes(new Point(x, y),new Dimension(w, h));
                        Rpolygon.setCotes(PolygoneCotes);
                        formeActuelle = Rpolygon;
                        Rpolygon.draw(g);
                        break;
                    case Ellipse:
                        Ellipse ellipse = new Ellipse();
                        ellipse.setProprietes(new Point(x, y),new Dimension(w, h));
                        formeActuelle = ellipse;
                        ellipse.draw(g);
                        break;
                    case Rectangle:
                        Rectangle rectangle = new Rectangle();
                        rectangle.setProprietes(new Point(x,y),new Dimension(w,h));
                        formeActuelle = rectangle;
                        rectangle.draw(g);
                        break;
                    case Ligne:
                        Ligne line = new Ligne();
                        line.setProprietes(new Point(x,y),new Dimension(w,h));
                        formeActuelle = line;
                        line.draw(g);
                        break;
                    case Text:
                        Texte texte = new Texte();
                        texte.setProprietes(new Point(x,y),new Dimension(w,h));
                        texte.setTexte(texteentered);
                        formeActuelle = texte;
                        texte.draw(g);
                        break;
                    case Propr:
                        if (formeSelectionne != null) {

                            formeSelectionne.setProprietes(new Point(x, y) ,new Dimension(w, h));

                        }
                        break;
                }
                if (formeActuelle != null) {
                    formes.add(formeActuelle);
                    formeActuelle = null;
                }

                // mettre à jour les formes
                Model.getModel().setFormes(formes);

                // mettre à jour l'historique
                Model.getModel().getHistorique().update();
            }




            MainGuiVue.dessinfield = 0;
        }


        if (currentX != -1 && currentY != -1) { //dessins des formes a la souris
            switch (MainGuiVue.getOperation()) {
                case Polygone:
                    Polygone Rpolygon = new Polygone();
                    Rpolygon.setProprietes(new Point(ancienX, ancienY),new Dimension(currentX - ancienX, currentY - ancienY));
                    Rpolygon.setCotes(PolygoneCotes);
                    formeActuelle = Rpolygon;
                    Rpolygon.draw(g);
                    break;
                case Rectangle:
                    Rectangle rectangle = new Rectangle();
                    rectangle.setProprietes(new Point(Math.min(ancienX, currentX), Math.min(ancienY, currentY)),new Dimension(calculateWidth(), calculateHeight()));
                    formeActuelle = rectangle;
                    rectangle.draw(g);
                    break;
                case Ellipse:
                    Ellipse ellipse = new Ellipse();
                    ellipse.setProprietes(new Point(Math.min(ancienX, currentX), Math.min(ancienY, currentY)),new Dimension(calculateWidth(), calculateHeight()));
                    formeActuelle = ellipse;
                    ellipse.draw(g);
                    break;
                case Ligne:
                    Ligne line = new Ligne();
                    line.setProprietes(new Point(ancienX,ancienY),new Dimension(currentX-ancienX,currentY-ancienY));
                    formeActuelle = line;
                    line.draw(g);
                    break;
                case Text:
                    Texte texte = new Texte();
                    texte.setProprietes(new Point(ancienX,ancienY),new Dimension(currentX-ancienX,currentY-ancienY));
                    texte.setTexte(texteentered);
                    formeActuelle = texte;
                    texte.draw(g);
                    break;
                case Redim:
                    if (formeSelectionne != null) {

                        formeSelectionne.redimensionner(ancienX, ancienY, currentX, currentY);

                        ancienX = currentX;
                        ancienY = currentY;
                    } else if (formeActuelle != null) {
                        formeActuelle.redimensionner(ancienX, ancienY, currentX, currentY);
                        ancienX = currentX;
                        ancienY = currentY;
                        formeActuelle.draw(g);
                    }
                    break;
                case Deplacer:
                    if (formeSelectionne != null) {

                        formeSelectionne.move(ancienX, ancienY, currentX, currentY);

                        ancienX = currentX;
                        ancienY = currentY;
                    } else if (formeActuelle != null) {
                        formeActuelle.move(ancienX, ancienY, currentX, currentY);
                        ancienX = currentX;
                        ancienY = currentY;
                        formeActuelle.draw(g);
                    }
                    break;


                case Selectionner:
                    break;
            }
        }


        BarCoord.liste.setText(BarCoordControl.recupformes());

        for (Formes forme : Model.getModel().getFormes()) {
            forme.draw(g);

        }

        grise = true;

        if(formeSelectionne != null)
            formeSelectionne.draw(g);

        grise = false;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if ( isMac()) {
            if (SwingUtilities.isRightMouseButton(e)) {
                xCliqueDroit = e.getX();
                yCliqueDroit = e.getY();
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {

        if (isWindows() || isUnix()) {


            if (SwingUtilities.isRightMouseButton(e)) {
                xCliqueDroit = e.getX();
                yCliqueDroit = e.getY();

            }
            if (!SwingUtilities.isLeftMouseButton(e))
                return;

        }
        else if ( isMac())
        {
            if (e.isPopupTrigger())
                return;
        }
        List<Formes> formes = Model.getModel().getFormes();
        ancienX = e.getX();
        ancienY = e.getY();
        currentX = ancienX;
        currentY = ancienY;
        switch (MainGuiVue.getOperation()) {

            case Redim:
                if (formeSelectionne == null)
                    for (int i = formes.size() - 1; i >= 0; i = i - 1) {
                        Formes fo = formes.get(i);
                        try {
                            if (fo.contain(currentX, currentY)) {
                                formeActuelle = fo;
                                formes.remove(fo);
                                break;

                            }
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }
                break;
            case Deplacer:
                if (formeSelectionne == null)
                    for (int i = formes.size() - 1; i >= 0; i = i - 1) {
                        Formes fo = formes.get(i);
                        try {
                            if (fo.contain(currentX, currentY)) {
                                formeActuelle = fo;
                                formes.remove(fo);
                                break;
                            }
                        } catch (Exception p) {
                            p.printStackTrace();
                        }
                    }

                break;
            case Remplir:
                if (formeSelectionne == null) {
                    for (Formes forme : formes) {
                        if (forme.contain(currentX, currentY)) {
                            forme.setFillColor(couleurSelect);
                            a_change = true;
                            break;
                        }
                    }
                }
                ToolBarControl.supprimerFormeSelect();
                break;

            case Selectionner:
                boolean formeTrouvee = false;

                for (int i = formes.size() - 1; i >= 0; i = i - 1) {
                    Formes forme = formes.get(i);
                    if (forme.contain(currentX, currentY)) {
                        formeSelectionne = forme;
                        BarCoord.x_box.setText(Integer.toString(forme.getLocation().x));    //BarCoord commentaire
                        BarCoord.y_box.setText(Integer.toString(forme.getLocation().y));
                        BarCoord.h_box.setText(Integer.toString(forme.getDimensions().height));
                        BarCoord.w_box.setText(Integer.toString(forme.getDimensions().width));

                        formeTrouvee = true;
                        break;
                    }
                }
                boolean selectedFound = false;
                if (!formeTrouvee) {
                    if(formeSelectionne != null) {
                        Formes forme = formeSelectionne;
                        if (forme.contain(currentX, currentY)) {
                            formes.add(forme);
                            formeSelectionne = null;
                            selectedFound = true;
                            break;
                        }


                        if (!formeTrouvee) {
                            formes.add(formeSelectionne);
                            formeSelectionne = null;
                        }
                    }
                }

                break;
        }
    }
    /**
     * la fonction SwingUtilities.isLeftMouseButton ne marchant pas correctement sur mac et inversement pour isPopupTrigger sur Windows, nous décidons de detecter le systeme d'exploitation
     */

    @Override
    public void mouseReleased(MouseEvent e) {

        if (isWindows()  || isUnix()) {

            if (!SwingUtilities.isLeftMouseButton(e))
                return;
        }
        else if ( isMac())
        {
            if (e.isPopupTrigger())
                return;
        }
        List<Formes> formes = Model.getModel().getFormes();

        switch (MainGuiVue.getOperation()) {

            case Suppr:


                for (int i = formes.size() - 1; i >= 0; i = i - 1) {
                    try {
                        Formes fo = formes.get(i);
                        if (fo.contain(currentX, currentY)) {
                            formes.remove(i);
                            a_change = true;
                            break;
                        }

                    } catch (Exception p) {
                    }

                }
                currentX = currentY = -1;
                repaint();

                break;
            default:
                if (formeActuelle != null) {
                    formes.add(formeActuelle);
                    formeActuelle = null;
                    a_change = true;
                }
        }
        currentX = currentY = ancienX = ancienY = -1;

        if (MainGuiVue.getOperation() != MainGuiVue.Operation.Selectionner && a_change) {
            // met à jour les formes
            Model.getModel().setFormes(formes);

            // met a jour l'historique
            Model.getModel().getHistorique().update(); //met a jour l'historique

            // reinitialise le clique
            a_change = false;
        }
        repaint();


    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mouseDragged(MouseEvent e) {

        if (isWindows() || isUnix()) {

            if (!SwingUtilities.isLeftMouseButton(e))
                return;
        }
        else if ( isMac())
        {
            if (e.isPopupTrigger())
                return;
        }
        currentX = e.getX();
        currentY = e.getY();
        repaint();
    }

    @Override
    public void mouseMoved(MouseEvent e) {
    }

    void doneSelecting() {
        grise = false;
        formeSelectionne = null;
    }

    private int calculateWidth() {
        return Math.abs(ancienX - currentX);
    }

    private int calculateHeight() {
        return Math.abs(ancienY - currentY);
    }

}