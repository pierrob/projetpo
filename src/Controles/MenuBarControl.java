/**
 * BONIJOL Pierre & LECOCQ Coraline
 */

package Controles;

import javax.swing.*;
import java.awt.*;

import Models.Couleur;
import Models.OuvertureFichiers;
import Vues.MainGuiVue;
import Vues.MenuBarVue;

import Vues.Sauvegarder;
import Vues.Ouvrir;

import Models.Model;

import exceptions.*;


/**
 * Controler la barre de menu
 */

public class MenuBarControl {

    private JMenuBar menuBar;


    public MenuBarControl() {
        menuBar = MenuBarVue.getMenuBar();


        addListners();
    }

    /**
     * Listeners pour les elements
     */
    private void addListners() {
        Component[] components = menuBar.getComponents();
        for (Component component : components) {
            Component[] menuComponents = ((JMenu) component).getMenuComponents();
            for (Component menuComponent : menuComponents) {
                if(menuComponent instanceof JSeparator) continue;
                JMenuItem menuItem = (JMenuItem) menuComponent;
                switch (menuItem.getText()) {

                    case "Ouvrir":
                        menuItem.addActionListener(e -> new Ouvrir());
                        break;

                    case "Sauvegarder":
                        menuItem.addActionListener(e -> new Sauvegarder());
                        break;

                    case "Redo":
                        menuItem.addActionListener(e -> {
                            Paint.getPainterPanel().doneSelecting();
                            try {
                                Model.getModel().getHistorique().redo();
                                Paint.getPainterPanel().repaint();
                            } catch (HistoryIsEmptyException e1) {
                                MainGuiVue.getMainGuiView().showError(e1.toString());
                            } catch (CannotRedoException e2) {
                                MainGuiVue.getMainGuiView().showError(e2.toString());
                            }
                        });
                        break;

                    case "Undo":
                        menuItem.addActionListener(e -> {
                            Paint.getPainterPanel().doneSelecting();
                            try {
                                Model.getModel().getHistorique().undo();

                                Paint.getPainterPanel().repaint();
                            } catch (HistoryIsEmptyException e1) {
                                MainGuiVue.getMainGuiView().showError(e1.toString());
                            } catch (CannotUndoException e2) {
                                MainGuiVue.getMainGuiView().showError(e2.toString());
                            }
                        });
                        break;

                    case "Insérer image":
                        menuItem.addActionListener(e -> new OuvertureFichiers());
                        break;

                    case "Couleur bg":
                        menuItem.addActionListener(e -> new Couleur(true));
                        break;
                }
            }
        }
    }

}
