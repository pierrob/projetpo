/**
 * BONIJOL Pierre & LECOCQ Coraline
 */

package Controles;


import Models.Model;
import Vues.BarCoord;
import Vues.MainGuiVue;
import formes.Formes;

import javax.swing.*;
import java.awt.*;
import java.util.List;


/**
 * classe de base pour les boutons d'opération
 */
public class BarCoordControl{

    private JPanel opeBarCoord;


    /**constructeur**/
    public BarCoordControl() {
        opeBarCoord = BarCoord.getopeBarCoord();
        addListners();

    }


    public static String recupformes(){ // renvoi un string avec les info des formes

        List<Formes> formes = Model.getModel().getFormes();
        String str = "";
        for (int i = formes.size() - 1; i >= 0; i = i - 1) {
            Formes fo = formes.get(i);
            str = str + fo.getClass().getName() + " "; //nom forme
            if(fo.getLocation() == null) continue;
            str = str + fo.getLocation().x  + " "; // x
            str = str + fo.getLocation().y  + " "; // y
            str = str + fo.getDimensions().width  + " "; // w
            str = str + fo.getDimensions().height  + " "; // h
            str = str + "\n";
        }
        return str;
    }

    private void addListners() {
        Component[] components = opeBarCoord.getComponents();
        for (Component component : components) {
            if (component instanceof JTextField || component instanceof JScrollPane) continue;
            JButton buttonComponent = (JButton) component;
            switch (buttonComponent.getText()) {

                case "Appliquer":
                    buttonComponent.addActionListener(e -> {


                        if(Paint.formeSelectionne != null)
                            MainGuiVue.setOperation(MainGuiVue.Operation.Propr);

                        if (MainGuiVue.getOperation() == null || MainGuiVue.getOperation() == MainGuiVue.Operation.Selectionner || MainGuiVue.getOperation() == MainGuiVue.Operation.Remplir || MainGuiVue.getOperation() == MainGuiVue.Operation.Suppr) {
                            MainGuiVue.getMainGuiView().showError("Veuillez selectionner une operation ou une forme");
                        } else {
                            try {
                                MainGuiVue.xfield = Integer.parseInt(BarCoord.x_box.getText());
                                MainGuiVue.yfield = Integer.parseInt(BarCoord.y_box.getText());
                                MainGuiVue.wfield = Integer.parseInt(BarCoord.w_box.getText());
                                MainGuiVue.hfield = Integer.parseInt(BarCoord.h_box.getText());



                                MainGuiVue.dessinfield = 1;
                                MainGuiVue.getMainGuiView().repaint();
                            } catch (NumberFormatException en) {
                                MainGuiVue.getMainGuiView().showError("Seulement int");

                            }
                        }

                    });
                    break;
            }
            MainGuiVue.getMainGuiView().repaint();
        }
    }



}