/**
 * BONIJOL Pierre & LECOCQ Coraline
 */

package Controles;

import Vues.ModifVue;
import Vues.MainGuiVue;

import Models.Model;
import Models.Couleur;

import javax.swing.*;
import java.awt.*;


/**
 * Controler la barre des modifications
 */
public class ModifControl {

    private JPanel oprationBarPanel;

    public ModifControl() {
        oprationBarPanel = ModifVue.getOprationBarPanel();
        addListners();
    }

    /**
     * Listener pour tout les objets de la barre
     */
    private void addListners() {
        Component[] components = oprationBarPanel.getComponents();
        for (Component component : components) {
            JButton buttonComponent = (JButton) component;
            switch (buttonComponent.getText()) {

                case "Supprimer":
                    buttonComponent.addActionListener(e -> {
                        MainGuiVue.setOperation(MainGuiVue.Operation.Suppr);

                        if (Paint.formeSelectionne != null)
                            Paint.formeSelectionne = null;

                        MainGuiVue.getMainGuiView().repaint();
                    });
                    break;
                case "Deplacer":
                    buttonComponent.addActionListener(e -> MainGuiVue.setOperation(MainGuiVue.Operation.Deplacer)
                    );
                    break;
                case "Redimensionner":
                    buttonComponent.addActionListener(e -> MainGuiVue.setOperation(MainGuiVue.Operation.Redim));
                    break;
                case "Couleurs":
                    buttonComponent.addActionListener(e -> new Couleur(false));
                    break;

                case "Selectionner":
                    buttonComponent.addActionListener(e -> {
                        MainGuiVue.setOperation(MainGuiVue.Operation.Selectionner);

                        if (Paint.formeSelectionne != null)
                            Model.getModel().getFormes().add(Paint.formeSelectionne);

                        Paint.formeSelectionne = null;

                        MainGuiVue.getMainGuiView().repaint();

                    });
                    break;
                case "Remplir":
                    buttonComponent.addActionListener(e -> {
                        MainGuiVue.setOperation(MainGuiVue.Operation.Remplir);
                        if (Paint.formeSelectionne != null)
                                Paint.formeSelectionne.setFillColor(Paint.couleurSelect);

                        MainGuiVue.getMainGuiView().repaint();
                    });
                    break;
            }
            MainGuiVue.getMainGuiView().repaint();
        }
    }
}
