/**
 * BONIJOL Pierre & LECOCQ Coraline
 */

package Controles;

import javax.swing.*;
import java.awt.*;

import Vues.BarCoord;
import Vues.ToolBarVue;
import Vues.MainGuiVue;


public class ToolBarControl {
    /**
     * Controler barre des formes
     */
    private JPanel toolBarPanel;

    public ToolBarControl() {
        toolBarPanel = ToolBarVue.getToolBarPanel();
        addListners();
    }

    /**
     * effacer forme selectionnée
     */
    public static void supprimerFormeSelect() {

        Paint.formeSelectionne = null;
        MainGuiVue.getMainGuiView().repaint();

    }

    private void addListners() {
        Component[] components = toolBarPanel.getComponents();
        for (Component component : components) {
            JButton buttonComponent = (JButton) component;
            switch (buttonComponent.getText()) {
                case "Polygone":
                    buttonComponent.addActionListener(e -> {
                        MainGuiVue.setOperation(MainGuiVue.Operation.Polygone);
                        try {
                            BarCoord.h_box.setText("H");
                            BarCoord.w_box.setText("W");
                            Paint.PolygoneCotes = Integer.parseInt(
                                    JOptionPane.showInputDialog("Entrer nombre de cotés "));
                        } catch (Exception p) {

                        }
                        supprimerFormeSelect();
                    });
                    break;
                case "Ligne":

                    buttonComponent.addActionListener(e -> {
                        BarCoord.h_box.setText("X Final");
                        BarCoord.w_box.setText("Y Final");
                        MainGuiVue.setOperation(MainGuiVue.Operation.Ligne);
                        supprimerFormeSelect();
                    });
                    break;
                case "Ellipse":
                    buttonComponent.addActionListener(e -> {
                        BarCoord.h_box.setText("H");
                        BarCoord.w_box.setText("W");
                        MainGuiVue.setOperation(MainGuiVue.Operation.Ellipse);
                        supprimerFormeSelect();
                    });
                    break;
                case "Rectangle":
                    buttonComponent.addActionListener(e -> {
                        BarCoord.h_box.setText("H");
                        BarCoord.w_box.setText("W");
                        MainGuiVue.setOperation(MainGuiVue.Operation.Rectangle);
                        supprimerFormeSelect();
                    });
                    break;
                case "Texte":
                    buttonComponent.addActionListener(e -> {
                        BarCoord.h_box.setText("H");
                        BarCoord.w_box.setText("W");
                        MainGuiVue.setOperation(MainGuiVue.Operation.Text);
                        try {
                            Paint.texteentered =
                                    JOptionPane.showInputDialog("Entrer texte ");
                        } catch (Exception p) {

                        }
                        supprimerFormeSelect();
                    });
                    break;


            }
        }
    }


}
