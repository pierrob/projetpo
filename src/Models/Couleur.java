/**
 * BONIJOL Pierre & LECOCQ Coraline
 */

package Models;


import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;

import Controles.Paint;
import Vues.MainGuiVue;

public class Couleur extends JFrame implements ChangeListener {

    private static JColorChooser tcc;
    private static boolean bg; // Si la couleur voulant etre changée est le fond

    public Couleur(boolean bg) {
        setTitle("Choisir une couleur");
        setVisible(true);
        setSize(600, 400);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this.bg = bg;
        initComponents();
    }

    private void initComponents() {

        JPanel bannerPanel = new JPanel(new BorderLayout());
        tcc = new JColorChooser();
        tcc.getSelectionModel().addChangeListener(this);

        this.add(bannerPanel, BorderLayout.CENTER);
        this.add(tcc, BorderLayout.NORTH);
        JButton btn = new JButton("Fermer");

        btn.addActionListener(arg0 -> dispose());

        bannerPanel.add(btn, BorderLayout.SOUTH);
    }

    //mettre a jour la couleur

    public void stateChanged(ChangeEvent e) {
        if(this.bg)
            MainGuiVue.painterPanel.setBackground(tcc.getColor());
            else
             Paint.couleurSelect = tcc.getColor();

    }
}