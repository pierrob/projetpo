/**
 * BONIJOL Pierre & LECOCQ Coraline
 */

package Models;

import exceptions.*;

import formes.Formes;

import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.List;
import java.util.Stack;


public class Historique {

    /**
     *  Class pour stocker les formes
     */


    /**
     *   pile contenant les dernieres infos
     */
    private Stack<List<Formes>> pileHistorique;

    /**
     *  pour revenir en avant
     */

    private Stack<List<Formes>> autrePile;

    public Historique() {
        pileHistorique = new Stack<>();
        pileHistorique.push(new ArrayList<>());
        autrePile = new Stack<>();
    }


    /**
     *   refaire action supprimée
     */

    public void redo() throws CannotRedoException, HistoryIsEmptyException {
        if (autrePile.isEmpty() && pileHistorique.isEmpty()) {
            throw new HistoryIsEmptyException();
        } else {
            try {
                List<Formes> formes = pileHistorique.push(autrePile.pop());
                Model.getModel().setFormes(Model.getModel().cloneFormes(formes));
            } catch (EmptyStackException e) {
                throw new CannotRedoException();
            }
        }

    }

    /**
     *  supprimmer derniere action
     */
    public void undo() throws CannotUndoException, HistoryIsEmptyException{
        if (autrePile.isEmpty() && pileHistorique.isEmpty()) {
            throw new HistoryIsEmptyException();
        } else {
            try {
                autrePile.push(pileHistorique.pop());
                List<Formes> formes = pileHistorique.peek();
                Model.getModel().setFormes(Model.getModel().cloneFormes(formes));
            } catch (EmptyStackException e) {
                throw new CannotUndoException();
            }
        }
    }
    /**
     *  mettre à jour l'historique
     */

    public void update() {

        pileHistorique.push(Model.getModel().cloneFormes(Model.getModel().getFormes()));
        autrePile.clear();

    }



}
