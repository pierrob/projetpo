/**
 * BONIJOL Pierre & LECOCQ Coraline
 */

package Models;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import Vues.MainGuiVue;

@SuppressWarnings("serial")
public class OuvertureFichiers extends JFrame {
    public static BufferedImage image;

    public OuvertureFichiers() {
        JFileChooser openFile = new JFileChooser();
        int result = openFile.showOpenDialog(null);
        if (result == JFileChooser.APPROVE_OPTION) {
            if (openFile.getSelectedFile().getName().endsWith(".png") || openFile.getSelectedFile().getName().endsWith(".jpg") || openFile.getSelectedFile().getName().endsWith(".jpeg")) {
                try {
                    image = ImageIO.read(openFile.getSelectedFile());
                    MainGuiVue.getMainGuiView().repaint();
                } catch (IOException ex) {
                }
            }
            else {
                MainGuiVue.getMainGuiView().showError("Veuillez selectionner un png/jpeg/jpg");
            }
            }
        }
}
