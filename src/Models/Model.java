/**
 * BONIJOL Pierre & LECOCQ Coraline
 */

package Models;

import formes.Formes;

import java.util.ArrayList;
import java.util.List;


public class Model {
    private static Model model;
    private List<Formes> formes;
    /**
     * Déclare l'historique
     */
    private Historique historique;

    private Model() {
        historique = new Historique();
        formes = new ArrayList<>();
    }

    /**
     * DRetourne l'historique
     */
    public Historique getHistorique() {
        return historique;
    }

    /**
     * Retourne la liste des formes
     */
    public List<Formes> getFormes() {
        return formes;
    }

    /**
     * Retourne le model courant
     */
    public static Model getModel() {
        if (model == null) {
            model = new Model();
        }
        return model;
    }

    /**
     * Met a jour les formes
     */
    public void setFormes(List<Formes> formes) {
        this.formes = formes;
    }

    /**
     * Copie de la liste des formes
     */
    public List<Formes> cloneFormes(List<Formes> formes) {
        List<Formes> formeClonees = new ArrayList<>();
        for (Formes forme : formes) {
            try {
                formeClonees.add((Formes) forme.clone());
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }
        }
        return formeClonees;
    }


}

