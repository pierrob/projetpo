/**
 * BONIJOL Pierre & LECOCQ Coraline
 */

package Vues;

import java.awt.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import Models.Model;

import formes.Formes;

import javax.swing.*;
import java.io.File;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;



@SuppressWarnings("serial")
public class Sauvegarder extends JFrame {

    /**
     * sauvegarde dans le xml
     */
    private static Document document = null;
    private static synchronized void sauvegarder(String path, List<Formes> formes) {

        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            document = dBuilder.newDocument();
            ecrireFormes(formes);
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(document);
            StreamResult result = new StreamResult(new File(path));
            transformer.transform(source, result);
        } catch (Exception e) {
            e.printStackTrace();
        }
        document = null;
    }
    /**
     * ecrit lecrit les formes
     */
    private static void ecrireFormes(List<Formes> formes) {
        Element rootElement = document.createElement("formes");
        document.appendChild(rootElement);
        for (Formes forme : formes) {
            ecrireForme(forme, rootElement);
        }
    }
    /**
     * ecrit les proprietes de la forme (x,y,w,h)
     */
    private static void ecrireProp(Point point,Dimension dim,Element rootElement)  {
        Element pointElement = document.createElement("point");
        Element dimElement = document.createElement("dimensions");


            pointElement.setAttribute("x", String.valueOf(point.x));
            pointElement.setAttribute("y", String.valueOf(point.y));
            dimElement.setAttribute("w", String.valueOf(dim.width));
            dimElement.setAttribute("h", String.valueOf(dim.height));


        rootElement.appendChild(pointElement);
        rootElement.appendChild(dimElement);
    }

    /**
     * ecrit la couleur de la forme
     */
    private static void ecrireCouleurs(Color color,Element rootElement, boolean fill)  {
        Element colorElement;
        if (fill)
             colorElement = document.createElement("fillcolor");
        else
            colorElement = document.createElement("color");



            colorElement.setAttribute("red", String.valueOf(color.getRed()));
            colorElement.setAttribute("green", String.valueOf(color.getGreen()));
            colorElement.setAttribute("blue", String.valueOf(color.getBlue()));

        rootElement.appendChild(colorElement);

    }

    /**
     * ecrit le texte si la forme est de type Texte
     */
    private static void ecrireTexte(String txt,Element rootElement)  {
        Element txtElement;

        txtElement = document.createElement("texte");



        txtElement.setAttribute("txt", txt);

        rootElement.appendChild(txtElement);

    }
    /**
     * retourne le texte si la forme est de type Texte
     */
    private static String textRetourne(formes.Texte txt)
    {
        return txt.getTexte();
    }

    /**
     * ecrit la forme dans le xml
     */
    private static void ecrireForme(Formes forme, Element rootElement) {
        Element element = document.createElement("forme");


        element.setAttribute("class", forme.getClass().getName());
            ecrireProp(forme.getLocation(),forme.getDimensions(), element);
            ecrireCouleurs(forme.getColor(), element,false);
            if (forme.getFillColor() != null)
                ecrireCouleurs(forme.getFillColor(), element,true );
            if(forme.getClass().getName().equals("formes.Texte"))
                ecrireTexte(textRetourne((formes.Texte)forme),element);

        rootElement.appendChild(element);
    }
    /**
     * fenetre de navigation pour sauvegarder le fichier
     */
    public Sauvegarder() {


        JFileChooser saveFile = new JFileChooser();
        int sf = saveFile.showSaveDialog(saveFile);
        if (sf == JFileChooser.APPROVE_OPTION) {
            String ext = "";
            String extension = saveFile.getFileFilter().getDescription();
          if (extension.equals(".xml,.XML")||saveFile.getSelectedFile().toString().endsWith(".xml")) {
                ext = ".xml";
                if(!saveFile.getSelectedFile().toString().endsWith(ext))
                    saveFile.setSelectedFile(new File(saveFile.getSelectedFile().toString() + ext));
                sauvegarder(saveFile.getSelectedFile().toString(),Model.getModel().getFormes());

            }
            else
          {

              MainGuiVue.getMainGuiView().showError("Veuillez enregistrer en .xml");

          }
        }
    }
}