/**
 * BONIJOL Pierre & LECOCQ Coraline
 */

package Vues;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.IOException;


public class MenuBarVue {
    private static JMenuBar menuBar;
    private JMenu fichier;
    private JMenu edit;
    private JMenuItem ouvrir;
    private JMenuItem sauvegarder;
    private JMenuItem redo;
    private JMenuItem undo;
    private JMenuItem image;
    private JMenuItem couleurbg;

    private MenuBarVue() {
        menuBar = new JMenuBar();
        initComponent();
        buildMenuBar();
    }

    public static JMenuBar getMenuBar() {
        if (menuBar == null) {
            new MenuBarVue();
        }
        return menuBar;
    }

    private void initComponent() {

        fichier = new JMenu("Fichier");
        edit = new JMenu("Editer");

        menuBar = new JMenuBar();



        ouvrir = new JMenuItem("Ouvrir");
        sauvegarder = new JMenuItem("Sauvegarder");

        redo = new JMenuItem("Redo");

        undo = new JMenuItem("Undo");
        redo = new JMenuItem("Redo");
        image = new JMenuItem("Insérer image");
        couleurbg = new JMenuItem("Couleur bg");


        setIcones();
    }

    private void buildMenuBar() {

        fichier.add(ouvrir);
        fichier.add(sauvegarder);

        edit.add(undo);
        edit.add(redo);
        edit.add(image);
        edit.add(couleurbg);


        menuBar.add(fichier);
        menuBar.add(edit);
    }

    private void setIcones() {
        try {

            Image undoIcone = ImageIO.read(getClass().getResource("/menubar/undo.png"));
            Image redoIcone = ImageIO.read(getClass().getResource("/menubar/redo.png"));
            Image imageIcone = ImageIO.read(getClass().getResource("/menubar/dossier.png"));
            Image couleurIcone = ImageIO.read(getClass().getResource("/toolbar/palette.png"));
            Image ouvrirIcone = ImageIO.read(getClass().getResource("/menubar/ouvrir.png"));
            Image sauvegarderIcone = ImageIO.read(getClass().getResource("/menubar/sauvegarder.png"));

            undo.setIcon(new ImageIcon(undoIcone));
            redo.setIcon(new ImageIcon(redoIcone));
            image.setIcon(new ImageIcon(imageIcone));
            ouvrir.setIcon(new ImageIcon(ouvrirIcone));
            sauvegarder.setIcon(new ImageIcon(sauvegarderIcone));
            couleurbg.setIcon(new ImageIcon(couleurIcone));

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

}