/**
 * BONIJOL Pierre & LECOCQ Coraline
 */

package Vues;

import Controles.Paint;

import javax.swing.*;
import java.awt.*;


public class MainGuiVue extends JFrame {  //fenetre principale

    private static MainGuiVue mainGuiView;

    private static Operation operation = Operation.Ligne;//operation par defaut
    public static JScrollPane defiler;
    public static JPanel painterPanel;


    public enum Operation {
        Redim,
        Suppr,
        Deplacer,
        Remplir,
        Selectionner,
        Ellipse,
        Text,
        Propr,
        Polygone,
        Ligne,
        Rectangle

    }

    public static int dessinfield = 0,xfield = 0,yfield = 0,hfield = 0,wfield = 0;

    private MainGuiVue() {

        setTitle("Projet PO");

        setSize(1000, 800);
        setResizable(true);

        setMinimumSize(this.getSize());
        setLocation(100, 0);
        initComponents();
    }

    public static MainGuiVue getMainGuiView() {
        if(mainGuiView == null) {
            mainGuiView = new MainGuiVue();

        }
        return mainGuiView;
    }

    /**Affichage des barres*/
    private void initComponents() {
        defiler = new JScrollPane(); //Jscrollpane
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        JMenuBar menuBar = MenuBarVue.getMenuBar();
        JPanel toolBarPanel = ToolBarVue.getToolBarPanel();
        JPanel operationBarPanel = ModifVue.getOprationBarPanel();
        painterPanel = Paint.getPainterPanel();
        JPanel coordBarPanel  = BarCoord.getopeBarCoord();


        setJMenuBar(menuBar);
        add(toolBarPanel);
        add(operationBarPanel);
        add(coordBarPanel);
        defiler.setViewportView(BarCoord.liste); //pour defiler sur la liste
        coordBarPanel.add(defiler);
        add(painterPanel);


        FlowLayout flowLayout = (FlowLayout) toolBarPanel.getLayout();

        flowLayout.setAlignment(FlowLayout.LEFT);

        toolBarPanel.setBackground(Color.darkGray);

        add(toolBarPanel, BorderLayout.NORTH);

        operationBarPanel.setLayout(new BoxLayout(operationBarPanel,BoxLayout.Y_AXIS));
        operationBarPanel.setBackground(Color.darkGray);
        add(operationBarPanel, BorderLayout.EAST);



        FlowLayout flowLayout2 = (FlowLayout) coordBarPanel.getLayout();
        flowLayout2.setAlignment(FlowLayout.CENTER);
        coordBarPanel.setBackground(Color.darkGray);
        add(coordBarPanel, BorderLayout.SOUTH);

    }


    public void showError(String message) {
        JOptionPane.showMessageDialog(null, message, "Erreur", JOptionPane.ERROR_MESSAGE);
    }


    public static Operation getOperation() {
        return operation;
    }

    public static void setOperation(Operation operation) {
        MainGuiVue.operation = operation;
    }


}