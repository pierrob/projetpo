/**
 * BONIJOL Pierre & LECOCQ Coraline
 */

package Vues;


import java.awt.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import Models.Model;

import formes.Formes;

import javax.swing.*;
import java.io.File;
import java.util.LinkedList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.NodeList;

@SuppressWarnings("serial")
public class Ouvrir extends JFrame {

    /**
     * charge le fichier xml
     */

    private static synchronized List<Formes> charger(String path) {
        List<Formes> formes = new LinkedList<>();
        Document document = null;
        try {
            File inputFile = new File(path);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            document = dBuilder.parse(inputFile);
            document.getDocumentElement().normalize();
            NodeList nList = document.getElementsByTagName("forme");
            int size = nList.getLength();
            for (int i = 0; i < size; i++) {
                formes.add(lectureForme((Element)nList.item(i)));
            }
        } catch (Exception e) {
        }
        document = null;
        return formes;
    }

    /**
     * Recupere x et y des formes
     */

    private static Point lecturePoint(Element pointElement){

        int x = Integer.parseInt(pointElement.getAttribute("x"));
        int y = Integer.parseInt(pointElement.getAttribute("y"));
        return new Point(x,y);
    }

    /**
     * Recupere w et h des formes
     */

    private static Dimension lectureDim(Element pointElement){

        int w = Integer.parseInt(pointElement.getAttribute("w"));
        int h = Integer.parseInt(pointElement.getAttribute("h"));
        return new Dimension(w,h);
    }

    /**
     * Recupere la couleur
     */

    private static Color lectureCouleur(Element colorElement){

        int red = Integer.parseInt(colorElement.getAttribute("red"));
        int green = Integer.parseInt(colorElement.getAttribute("green"));
        int blue = Integer.parseInt(colorElement.getAttribute("blue"));
        return new  Color(red, green, blue);
    }

    /**
     * Recupere et appliques le texte si la forme chargée est de type Texte
     */

    private static String lectureTexte(Element colorElement){

        String strg = colorElement.getAttribute("txt");
        return strg;
    }
    private static void setTexte(formes.Texte forme, String txt)
    {
        forme.setTexte(txt);
    }

    /**
     * Ecris les parametres de la forme
     */

    private static Formes lectureForme(Element forme) {

        String formeClass = forme.getAttribute("class");
        Formes laForme = Formes.formesType(formeClass);
        Point position = lecturePoint((Element) forme.getElementsByTagName("point").item(0));
        Dimension dim = lectureDim((Element) forme.getElementsByTagName("dimensions").item(0));
        Color color = lectureCouleur((Element) forme.getElementsByTagName("color").item(0));
        if(formeClass.equals("formes.Texte")) {
            String texte = lectureTexte((Element) forme.getElementsByTagName("texte").item(0));
            setTexte((formes.Texte)laForme,texte);
        }


        if(position != null)
            laForme.setLocation(position);

        if(dim != null)
            laForme.setDimensions(dim);

        if(color != null)
            laForme.setColor(color);

        if (forme.getElementsByTagName("fillcolor").item(0) != null) {

            Color fillColor = lectureCouleur((Element) forme.getElementsByTagName("fillcolor").item(0));

            laForme.setFillColor(fillColor);
        }


        return laForme;
    }

    /**
     * fenetre de navigation pour trouver le fichier
     */
    public Ouvrir() {
        JFileChooser openFile = new JFileChooser();
        int result = openFile.showOpenDialog(null);
        if (result == JFileChooser.APPROVE_OPTION) {
     if (openFile.getSelectedFile().getName().endsWith(".xml")) {
                try {

                   Model.getModel().setFormes(charger(openFile.getSelectedFile().getPath()));
                    MainGuiVue.getMainGuiView().repaint();

                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
     else {
         MainGuiVue.getMainGuiView().showError("Veuillez ouvrir un XML");
     }
        }
    }
}