/**
 * BONIJOL Pierre & LECOCQ Coraline
 */

package Vues;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * classe de base pour les boutons d'opération
 */
public class ModifVue extends JPanel {

    private static ModifVue operationBarPanel;
    /**
     * Liste des boutons
     */
    private List<JButton> operationButtons;

    /**
     * redimensionner le bouton d'opération de forme.
     */
    private JButton resizeButton;
    /**
     * supprimer le bouton d'opération de forme.
     */
    private JButton deleteButton;
    /**
     * bouger le bouton d'opération de forme
     */
    private JButton moveButton;
    /**
     * bouton d'opération du sélecteur de couleur.
     */
    private JButton colorButton;
    /**
     * sélectionnez le bouton d'opération de forme.
     */
    private JButton selectButton;
    /**
     * remplir le bouton d'opération de forme.
     */
    private JButton fillShapeButton;


    private ModifVue() {
        initComponents();
        buildToolBar();
    }



    public static ModifVue getOprationBarPanel() {
        if (operationBarPanel == null) {
            operationBarPanel = new ModifVue();
        }
        return operationBarPanel;
    }

    private void initComponents() {
        operationButtons = new ArrayList<>();

        resizeButton = new JButton("Redimensionner");
        deleteButton = new JButton("Supprimer");
        moveButton = new JButton("Deplacer");
        colorButton = new JButton("Couleurs");
        selectButton = new JButton("Selectionner");
        fillShapeButton = new JButton("Remplir");

        IconesBouttons();

        operationButtons.add(moveButton);
        operationButtons.add(resizeButton);
        operationButtons.add(deleteButton);
        operationButtons.add(colorButton);
        operationButtons.add(selectButton);
        operationButtons.add(fillShapeButton);

    }

    private void IconesBouttons() {
        try {
            Image resizeIcon = ImageIO.read(getClass().getResource("/toolbar/redimensionner.png"));
            Image moveIcon = ImageIO.read(getClass().getResource("/toolbar/deplacer.png"));
            Image deleteIcon = ImageIO.read(getClass().getResource("/toolbar/corbeille.png"));
            Image colorChooserIcon = ImageIO.read(getClass().getResource("/toolbar/palette.png"));
            Image selectIcon = ImageIO.read(getClass().getResource("/toolbar/pointer.png"));
            Image fillIcon = ImageIO.read(getClass().getResource("/toolbar/remplir.png"));

            resizeButton.setIcon(new ImageIcon(resizeIcon));
            deleteButton.setIcon(new ImageIcon(deleteIcon));
            moveButton.setIcon(new ImageIcon(moveIcon));
            colorButton.setIcon(new ImageIcon(colorChooserIcon));
            selectButton.setIcon(new ImageIcon(selectIcon));
            fillShapeButton.setIcon(new ImageIcon(fillIcon));

        } catch (IOException ex) {

        }
    }

    /**
     * creation de la bar
     */
    private void buildToolBar() {
        for (JButton button : operationButtons) {
            add(button);
        }
        revalidate();
        repaint();
    }
}