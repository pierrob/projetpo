/**
 * BONIJOL Pierre & LECOCQ Coraline
 */

package Vues;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ToolBarVue extends JPanel {
    private static ToolBarVue toolBarPanel;
    private List<JButton> buttons;
    private JButton drawLineButton;
    private JButton drawOvalButton;
    private JButton drawRectButton;
    private JButton drawRegularPolygon;
    private JButton drawTexte;

    private ToolBarVue() {
        initComponents();
        buildToolBar();
    }

    public static ToolBarVue getToolBarPanel() {
        if (toolBarPanel == null) {
            toolBarPanel = new ToolBarVue();
        }
        return toolBarPanel;
    }

    private void initComponents() {
        buttons = new ArrayList<>();
         drawLineButton = new JButton("Ligne");
         drawOvalButton = new JButton("Ellipse");
         drawRectButton = new JButton("Rectangle");
         drawRegularPolygon=new JButton("Polygone");
         drawTexte=new JButton("Texte");



        buttons.add(drawLineButton);
        buttons.add(drawOvalButton);
        buttons.add(drawRectButton);
        buttons.add(drawRegularPolygon);
        buttons.add(drawTexte);

        setIcones();

    }

    private void buildToolBar() {
        for (JButton button : buttons) {
            add(button);
            revalidate();
            repaint();
        }
    }

    private void setIcones() {
        try {
            Image ellipseIcone = ImageIO.read(getClass().getResource("/toolbar/ellipse.png"));
            Image rectIcone = ImageIO.read(getClass().getResource("/toolbar/rectangle.png"));
            Image lineIcone = ImageIO.read(getClass().getResource("/toolbar/ligne.png"));
            Image polygonIcone = ImageIO.read(getClass().getResource("/toolbar/polygone.png"));
            Image texteIcone = ImageIO.read(getClass().getResource("/toolbar/texte.png"));

            drawLineButton.setIcon(new ImageIcon(lineIcone));
            drawOvalButton.setIcon(new ImageIcon(ellipseIcone));
            drawRectButton.setIcon(new ImageIcon(rectIcone));
            drawRegularPolygon.setIcon(new ImageIcon(polygonIcone));
            drawTexte.setIcon(new ImageIcon(texteIcone));

        } catch (IOException ex) {

        }
    }

}
