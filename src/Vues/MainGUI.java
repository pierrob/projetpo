package Vues;

import Controles.BarCoordControl;
import Controles.MenuBarControl;
import Controles.ModifControl;
import Controles.ToolBarControl;
import Models.Model;

/**
 BONIJOL Pierre & LECOCQ Coraline L3 MIAGE
 **/
public class MainGUI {

        private MainGuiVue mainGuiView;

    public MainGUI() {

            Thread runThread = new Thread(() -> {
                // Charger model
                Model.getModel();

                // charger GUI
                mainGuiView = MainGuiVue.getMainGuiView();

                // afficher le GUI après son chargement

                mainGuiView.setVisible(true);

                // charger les Controles
                new ToolBarControl();
                new ModifControl();
                new BarCoordControl();
                new MenuBarControl();

            });
            runThread.start();

        }

}
