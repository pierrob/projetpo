/**
 * BONIJOL Pierre & LECOCQ Coraline
 */

package Vues;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;



/**
 * classe de base pour les boutons d'opération
 */
public class BarCoord extends JPanel {

    private static BarCoord operationBarCoord;
    /**
     * Liste des boutons
     */
    private List<JButton> operationButtonsCoord;
    private List<JTextField> modifChamps;

    public static JTextArea liste;

    public static JTextField x_box;
    public static JTextField y_box;
    public static JTextField w_box;
    public static JTextField h_box;

    private JButton appliquer;



    /**constructeur**/
    private BarCoord() {

        initComponents();
        buildCoordBar();

    }

    public static BarCoord getopeBarCoord() {
        if (operationBarCoord == null) {
            operationBarCoord = new BarCoord();
        }
        return operationBarCoord;
    }


    private void initComponents() {
        operationButtonsCoord = new ArrayList<>();
        modifChamps = new ArrayList<>();

        x_box = new JTextField("",5);
        x_box.setText("X");
        y_box = new JTextField("",5);
        y_box.setText("Y");
        w_box = new JTextField();
        w_box.setText("W ou X final ");
        h_box = new JTextField();
        h_box.setText("H ou Y final");
        appliquer = new JButton("Appliquer");

        IconesBouttons();

        operationButtonsCoord.add(appliquer);

        modifChamps.add(x_box);
        modifChamps.add(y_box);
        modifChamps.add(h_box);
        modifChamps.add(w_box);

    }

    private void IconesBouttons() {
        try {
            Image appliquerIcone = ImageIO.read(getClass().getResource("/toolbar/appliquer.png"));

            appliquer.setIcon(new ImageIcon(appliquerIcone));

        } catch (IOException ex) {

        }
    }

    /**
     * creation de la bar
     */
    private void buildCoordBar() {
        liste =  new JTextArea ( 5 , 22 );
        for (JButton button : operationButtonsCoord) {
            add(button);
        }
        for (JTextField textfield : modifChamps)
        {
            textfield.setMaximumSize( new Dimension(300,30));
            add(textfield);

        }
        add(liste);

        revalidate();
        repaint();
    }


}